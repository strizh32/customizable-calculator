'use strict';

window.operations = [
    {
        name: 'sum',
        sign: '+',
        handler: (leftOperandValue, rightOperandValue) => {
            return +leftOperandValue + +rightOperandValue;
        },
        enabled: true,
    },

    {
        name: 'subtract',
        sign: '-',
        handler: (leftOperandValue, rightOperandValue) => {
            return +leftOperandValue - +rightOperandValue;
        },
        enabled: true,
    },

    {
        name: 'multiply',
        sign: '*',
        handler: (leftOperandValue, rightOperandValue) => {
            if ([leftOperandValue, rightOperandValue].includes(0)) {
                if (!confirm('Умножение на 0 всегда будет 0, продолжить вычисление?')) {
                    return;
                }
            }
            return +leftOperandValue * +rightOperandValue;
        },
        enabled: true,
    },

    {
        name: 'divide',
        sign: '/',
        handler: (leftOperandValue, rightOperandValue) => {
            if (rightOperandValue === 0) {
                return alert('На ноль делить нельзя. Введите второе значение, не равное нулю.');
            }
            return +leftOperandValue / +rightOperandValue;
        },
        enabled: true,
    },
];
